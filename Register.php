<?php
require_once 'Dbconfig.php';

if($_POST)
{
    $username= $_POST['user_name'];
    $password= $_POST['password'];
    try
    {
		$query = "select * from users WHERE username=?";  
		$stmt = $conn->prepare( $query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));  
		$stmt->execute(array($username));  
		$count=$stmt->rowCount(); 
        if($count==0){

			$query = "insert into users(username, password) values(?, ?)";  
			$stmt = $conn->prepare( $query, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );  
			if($stmt->execute( array( $username, $password ) ))
			{
				echo "1";
			}  
			else{
				echo "Kayıt sırasında bir hata oluştu.";
			}
        }
        else{

            echo "Lütfen başka bir kullanıcı adı belirleyiniz."; 
        }

    }
    catch(PDOException $e){
        echo $e->getMessage();
    }
}
?>