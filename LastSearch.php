<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>
<body>
<div style="margin:0px;padding:0px;background-color:rgb(45,45,79);height:25px">
	<a href="#" onclick="geridon3()" style="float:left;margin-left:10px;padding-top:5px;"><i class="fa fa-arrow-circle-left " style="color:white" aria-hidden="true"></i></a>
	<a href="#"  style="float:right;margin-right:10px;padding-top:5px;"><i class="fa fa-arrow-circle-right" style="color:gray" aria-hidden="true"></i></a>
</div>
<div class="container">
	<div class="login-container">
		<div class="avatar"><center><h1>İlaç Rehberim</h1></center></div>
		<div class="form-box">
			<ul style="text-align:left;padding:0px;margin:0px;">
				<?php
					session_start();
					require_once 'Dbconfig.php';
					try
					{
						$username=$_SESSION['Kullanici'];
						$query = "select * from users WHERE username=?";  
						$stmt = $conn->prepare( $query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));  
						$stmt->execute(array($username));  
						while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){  
						   $userID=$row["id"];
						}  

						$query = "select * from oncekiBaktigimIlaclar WHERE userID=?";  
						$stmt = $conn->prepare( $query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));  
						$stmt->execute(array($userID));  
						while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){  
							echo '<li><a href="'.$row["ilacUrl"].'">'.$row["ilacAdi"].'</a></li>';  
						}  
					}
					catch(PDOException $e){
						echo $e->getMessage();
					}
				?>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript" charset="utf-8" src="js.js"></script>
</body>
</html>

