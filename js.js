$(document).ready(function() {
	$('#KayitOlBtn').click(function(e){
		e.preventDefault();

		var user_name = $("#user_name").val();
		var password = $("#password").val();
		var cpassword = $("#cpassword").val();
		if (user_name=="" || user_name.length<5){
			$('#error').html("Kullanıcı adınız minimum 5 karakterden olmalıdır !");
		}
		else if (password=="" || user_name.length<6){
			$('#error').html("Parolanız minimum 6 karakterden olmalıdır !");
		}
		else if (cpassword=="" || user_name.length<6){
			$('#error').html("Parolanız minimum 6 karakterden olmalıdır !");
		}
		else if (cpassword!=password){
			$('#error').html("Parolalar uyuşmuyor !");
		}
		else{
			$.ajax({
				type: "POST",
				url: "Register.php",
				data: {user_name:user_name, password:password},
				success : function(data){
					if(data==1){
						$('#error').html("Kaydınız başarılı bir şekilde oluşturuldu. Lütfen giriş yapınız. 5 saniye içinde yönlendiriliyorsunuz.");
					   setTimeout(function () {
						   window.location.href = "http://finansalbtweb.azurewebsites.net/index.php"; 
						}, 5000); 
					}
					else{
						$('#error').html(data);
					}
					
				}
			});
		}
	});
	$('#GirisBtn').click(function(e){
		e.preventDefault();
		 var form = $('#login-form');
		$.ajax({
			type: "POST",
			url: "Login.php",
			data: form.serialize(),
			success : function(data){
				if(data==1){
					$('#Giriserror').html("Başarılı bir şekilde giriş yaptınız. 5 saniye içinde yönlendiriliyorsunuz.");
					   setTimeout(function () {
						   window.location.href = "http://finansalbtweb.azurewebsites.net/index.php"; 
						}, 5000); 
				}
				else if(data==0){
					$('#Giriserror').html("Lütfen Bilgilerinizi Kontrol Ediniz !");
				}
				else{
					alert(data);
				}
			}
		});

	});
	$('#logoff').click(function(e){
		e.preventDefault();
	   setTimeout(function () {
		   window.location.href = "http://finansalbtweb.azurewebsites.net/Logoff.php"; 
		}, 1000); 
	});
	$('#lastsearch').click(function(e){
		e.preventDefault();
	   setTimeout(function () {
		   window.location.href = "http://finansalbtweb.azurewebsites.net/LastSearch.php"; 
		}, 1000); 
	});
});

function ilacAra(){
		$("#anaform").hide(1000);
	    $("#searchilac").show(1000);
		$("#geriIcon").show();
}
function login(){
		$("#anaform").hide(1000);
	    $("#login").show(1000);
		$("#geriIcon").show();
}
function geridon(){
		$("#anaform").show(1000);
		$("#searchilac").hide(1000);
		 $("#login").hide(1000);
		 $("#girisYap").hide(1000);
		 $("#kayitOl").hide(1000);
		$("#geriIcon").hide();
}
function girisYap(){
		$("#login").hide(1000);
	    $("#girisYap").show(1000);
}
function kayitOl(){
		$("#login").hide(1000);
	    $("#kayitOl").show(1000);
}
function geridon2(){
	window.location='http://finansalbtweb.azurewebsites.net/index.php';

}
function geridon3(){
	    window.history.back();
}