<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>
<body>
<?php
	session_start();
	require_once 'Dbconfig.php';
function removeLink($str){
	$regex = '/<a (.*)<\/a>/isU';
	preg_match_all($regex,$str,$result);
	foreach($result[0] as $rs)
	{
		$regex = '/<a (.*)>(.*)<\/a>/isU';
		$text = preg_replace($regex,'$2',$rs);
		$str = str_replace($rs,$text,$str);
	}
	return $str;
}
function file_get_contents_utf8($fn) {
	$content = file_get_contents($fn);
	return mb_convert_encoding($content, 'UTF-8',
	mb_detect_encoding($content, 'UTF-8, ISO-8859-9', true));
}
function replace_tr($text) {
	$text = trim($text);
	$search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ');
	$replace = array('c','c','g','g','i','i','o','o','s','s','u','u','-');
	$new_text = str_replace($search,$replace,$text);
	return $new_text;
}  
function myUrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D','+');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "yuzde-", "#", "[", "]","-");
    return str_replace($entities, $replacements, urlencode($string));
}
echo '<div style="margin:0px;padding:0px;background-color:rgb(45,45,79);height:25px">';
echo '<a href="#" onclick="geridon3()" style="float:left;margin-left:10px;padding-top:5px;"><i class="fa fa-arrow-circle-left " style="color:white" aria-hidden="true"></i></a>';
echo '<a href="#"  style="float:right;margin-right:10px;padding-top:5px;"><i class="fa fa-arrow-circle-right" style="color:gray" aria-hidden="true"></i></a>';
echo '</div>';
	echo '<div class="col-md-12 col-sm-12 col-xs-12 ilacim">';
			
		$site2=file_get_contents_utf8("http://www.ilacprospektusu.com/ilac/419/".$_GET['adi']);
		preg_match_all('@<div class="prospektus" id="prospektus">(.*?)</div>@si',$site2,$baslik222);
		$ciktimm2=implode('',$baslik222[1]);
		echo removeLink($ciktimm2);
	echo '</div>';

try
{
	if (isset($_SESSION['Kullanici'])){
		$username=$_SESSION['Kullanici'];

		$query = "select * from users WHERE username=?";  
		$stmt = $conn->prepare( $query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));  
		$stmt->execute(array($username));  
		while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){  
		   $userID=$row["id"];
		}  

		$ilacUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$ilacAdi=$_GET['adi'];

		$query = "select * from oncekiBaktigimIlaclar WHERE userID=? AND ilacUrl=?";  
		$stmt = $conn->prepare( $query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));  
		$stmt->execute(array($userID,$ilacUrl));  
		$count=$stmt->rowCount(); 

		if($count==0){

			$query = "insert into oncekiBaktigimIlaclar(ilacAdi, ilacUrl, userID) values(?, ?, ?)";  
			$stmt = $conn->prepare( $query, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );  
			$stmt->execute( array( $ilacAdi, $ilacUrl, $userID ) );


	
		}
	}
}
catch(PDOException $e){
	echo $e->getMessage();
}	
?>
<script type="text/javascript" charset="utf-8" src="js.js"></script>
</body>
</html>
