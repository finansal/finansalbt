<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>
<body>
<div style="margin:0px;padding:0px;background-color:rgb(45,45,79);height:25px;display:none" id="geriIcon">
	<a href="#" onclick="geridon()" style="float:left;margin-left:10px;padding-top:5px;"><i class="fa fa-arrow-circle-left " style="color:white" aria-hidden="true"></i></a>
	<a href="#"  style="float:right;margin-right:10px;padding-top:5px;"><i class="fa fa-arrow-circle-right" style="color:gray" aria-hidden="true"></i></a>
</div>
<div class="container">
	<div class="login-container">
		<div class="avatar"><center><h1>İlaç Rehberim</h1></center></div>
		<div class="form-box" id="anaform">
			<button class="btn btn-info btn-block login" type="submit" onclick="ilacAra()" >İlaç Ara</button>
			<?php
				session_start();
				 if (!isset($_SESSION['Kullanici'])) {
					echo '<button class="btn btn-info btn-block login" type="submit" onclick="login()">Kayıt Ol / Giriş Yap</button>';
				}else if (isset($_SESSION['Kullanici'])){
					$username = $_SESSION['Kullanici'];
					echo '<button class="btn btn-info btn-block login" type="submit" id="lastsearch">Önceki Aramalarım</button>';
					echo '<button class="btn btn-info btn-block login" type="submit" id="logoff">Çıkış Yap</button>';
				}
			?>
		</div>
		<div class="form-box" id="searchilac">
			<form action="search.php" method="GET">
				<input name="ara"" type="text" placeholder="İlaç ara...( Majezik,Vermidon...)">
				<button class="btn btn-info btn-block login" type="submit">Ara</button>
			</form>
		</div>
		<div class="form-box" id="login">';
			<button class="btn btn-info btn-block login" type="submit" onclick="girisYap()" >Giriş Yap</button>
			<button class="btn btn-info btn-block login" type="submit" onclick="kayitOl()">Kayıt Ol</button>
		</div>
		<div class="form-box" id="girisYap">
			<form id="login-form">
				<div id="Giriserror"></div>
				<input type="text" name="girisUsername" placeholder="Kullanici Adi">
				<input type="password" name="girisParola" placeholder="Parola">
				<button class="btn btn-info btn-block login" type="submit" id="GirisBtn">Giriş Yap</button>
			</form>
		</div>
		<div class="form-box" id="kayitOl">
			<form id="register-form">
				<div id="error"></div>
				<input type="text" name="user_name" id="user_name" placeholder="Kullanici Adi">
				<input type="password" name="password" id="password" placeholder="Parola">
				<input type="password" name="cpassword" id="cpassword" placeholder="Tekrar Parola">
				<button class="btn btn-info btn-block login" type="submit" id="KayitOlBtn">Kayıt Ol</button>
			</form>
		</div>
	</div>
</div>

<script charset="UTF-8" type="text/javascript"  src="js.js"></script>
</body>
</html>


