<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>
<body>
<?php
function removeLink($str){
	$regex = '/<a (.*)<\/a>/isU';
	preg_match_all($regex,$str,$result);
	foreach($result[0] as $rs)
	{
		$regex = '/<a (.*)>(.*)<\/a>/isU';
		$text = preg_replace($regex,'$2',$rs);
		$str = str_replace($rs,$text,$str);
	}
	return $str;
}
function file_get_contents_utf8($fn) {
	$content = file_get_contents($fn);
	return mb_convert_encoding($content, 'UTF-8',
	mb_detect_encoding($content, 'UTF-8, ISO-8859-9', true));
}
function replace_tr($text) {
	$text = trim($text);
	$search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ');
	$replace = array('c','c','g','g','i','i','o','o','s','s','u','u','-');
	$new_text = str_replace($search,$replace,$text);
	return $new_text;
}  
function myUrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D','+');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "yuzde-", "#", "[", "]","-");
    return str_replace($entities, $replacements, urlencode($string));
}
$site=file_get_contents_utf8("http://www.ilacprospektusu.com/ara/ilac/adi/".myUrlEncode($_GET['ara']));
preg_match_all('@<div class="sonuclar">(.*?)</div>@si',$site,$baslik22);
$ciktimm=implode('',$baslik22[1]);
	echo '<center><h4 class="aranan"><span style="float:left;" onclick="geridon2()"><i class="fa fa-arrow-circle-left" style="color:white" aria-hidden="true"></i></span>Aranan : <strong>'.$_GET['ara'].'</strong><span style="float:right;"><i class="fa fa-arrow-circle-right" style="color:gray" aria-hidden="true"></i></span></h4></center>';
	echo '<div style="margin-top:20px;">';

		preg_match_all('/<a.*?>(.*?)<\/a>/', $ciktimm, $matches);
		$sayac=0;
		$i=1;
		foreach($matches[1] as $sehir) {
			if($sayac%2==0)
			{

				if(substr($sehir,2)!=""){
				echo "<div class='col-md-12 col-sm-12 col-xs-12 ilacc' style='margin-bottom:5px;'>";

						echo "<i class='fa fa-hand-o-right' style='color:rgb(24,137,190)' aria-hidden='true'></i>\t<a style='color:gray' href='ilac.php?adi=".strtolower(str_replace(")","",str_replace("(","",str_replace("/","-",str_replace("%","yuzde-",str_replace(",","-",str_replace(" ","-",substr(replace_tr($sehir),2))))))))."'>".substr($sehir,2)."</a>"."<span style='float:right;margin-right:10px;'><i class='fa fa-chevron-circle-right' style='color:rgb(24,137,190)' aria-hidden='true'></i></span>\t";
				echo "</div>";
				}
				$i++;
			}
			$sayac++;
		}
	echo '</div>';
?>
<script type="text/javascript" charset="utf-8" src="js.js"></script>
</body>
</html>
